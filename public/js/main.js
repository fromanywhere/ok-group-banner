document.addEventListener('DOMContentLoaded', function () {

	if (window.chrome) {
		var dropZone = document.documentElement;
		var uploadButton = document.querySelector('input[type="file"]');

		var dropOverlay = document.querySelector('.drop');

		document.addEventListener("dragover", function( event ) {
			event.dataTransfer.effectAllowed = 'copy';
			event.dataTransfer.dropEffect = 'copy';
			event.preventDefault();
		}, false);

		document.addEventListener("dragenter", function( event ) {
			dropZone.classList.add('__upload');
		}, false);

		document.addEventListener("dragleave", function( event ) {
			if (event.target == uploadButton) {
				dropZone.classList.remove('__upload');
			}
		}, false);

		document.addEventListener("drop", function( event ) {
			dropZone.classList.remove('__upload');
		}, false);

		uploadButton.addEventListener('change', function (e) {
			if (e.target.files.length > 1 || !(e.target.files[0].type === 'image/png' || e.target.files[0].type === 'image/jpeg') ) { console.log('!');
				uploadButton.value = '';
			}
		})
	}
});