var express = require('express');
var router = express.Router();
var multipart = require('connect-multiparty');
var fs = require('fs');
var gm = require('gm');
var execFile = require('child_process').execFile;
var jpegOptim = require('jpegoptim-bin').path;
var ejs = require('ejs');
var Promise = require('es6-promise').Promise;
var spawn = require('child_process').exec;
var sizeOf = require('image-size');

var multipartMiddleware = multipart();

/* File upload */

function cmd_exec(cmd, args, cb) {
	var child = spawn(cmd, args);
	child.stdout.on('end', function () { cb() });
}


function packImages (params) {
	return new Promise(function (resolve, reject) {
		var newParam = params;
		var zip = new cmd_exec('zip ' + params.name + '.zip *', {
				cwd: newParam.targetPath
			},
			function () {
				newParam.zip = params.absolutePath + params.name + '.zip';
				resolve(newParam);
			}
		);

	});
}

function generateHTML(params) {
	if (!params.originalFilename) {
		return false;
	}

	var size = sizeOf(params.originalFilename);

	var data = {
		title: params.name,
		iframeLink: params.link,
		newFeedBannerPath: params.files[params.uploadField].originalFilename.replace('.png', '.jpg'),
		width: size.width,
		height: size.height
	};

	var templateWithLink = fs.readFileSync(appRoot + '/serviceTemplates/index_with_link.ejs', 'utf8');
	var templateWithoutLink = fs.readFileSync(appRoot + '/serviceTemplates/index.ejs', 'utf8');

	var htmlWithLink = ejs.render(templateWithLink, data);
	var htmlWithoutLink = ejs.render(templateWithoutLink, data);

	fs.writeFileSync(params.targetPath + 'index_with_link.html', htmlWithLink);
	fs.writeFileSync(params.targetPath + 'index.html', htmlWithoutLink);

	return true;
}

function optimizeImage(params) {

	return new Promise(function (resolve, reject) {

		fs.writeFileSync(params.optimizedFilename, fs.readFileSync(params.originalFilename));

		var args = [
			'--strip-all',
			'--all-progressive',
			'--overwrite',
			params.optimizedFilename
		];

		execFile(jpegOptim, args, function (err) {
			if (err) {
				throw err;
			}

			fs.unlinkSync(params.originalFilename);
			fs.renameSync(params.optimizedFilename, params.originalFilename);

			resolve();
		});
	});
}

function convertImages(params) {

	return new Promise(function (resolve, reject) {

		var newParam = params;
		var fileProp = newParam.files[newParam.uploadField];

		if (fileProp.type == 'image/png') {
			gm(newParam.originalFilename)
				.flatten()
				.quality('99')
				.write(newParam.originalFilename.replace('.png', '.jpg'), function (err) {
					fs.unlinkSync(newParam.originalFilename);
					newParam.originalFilename = newParam.originalFilename.replace('.png', '.jpg');
					resolve(newParam);
				});

		} else {
			resolve(newParam);
		}
	});
}

function uploadImages(param) {

	var newParam = param;
	var fileProp = newParam.files[newParam.uploadField];

	var tempPath = fileProp.path;

	newParam.originalFilename = newParam.targetPath + fileProp.originalFilename;
	newParam.optimizedFilename = newParam.targetPath + 'opt_' + fileProp.originalFilename;

	if (!fs.existsSync(newParam.targetPath)) {
		fs.mkdirSync(newParam.targetPath);
	}

	fs.renameSync(tempPath, newParam.originalFilename);

	return newParam;
}

router.post('/', multipartMiddleware, function(req, res) {

	var parsedLink = req.body.link.split('/');
	parsedLink =  parsedLink[parsedLink.length-1];

	var absolutePath = '/uploads/' + parsedLink + '_' + Date.now() + '/';
	var targetPath = appRoot + '/public' + absolutePath;

	var params = {
        uploadField: 'newfeed_banner',
		files: req.files,
		absolutePath: absolutePath,
		targetPath: targetPath,
		link: req.body.link,
		name: parsedLink
	}

	params = JSON.parse(JSON.stringify(uploadImages(params)));

	convertImages(params).then(function (newParams) {
		params = JSON.parse(JSON.stringify(newParams));
		optimizeImage(params).then(function () {
			if (generateHTML(params)) {
				packImages(params).then(function (newParams) {
					params = JSON.parse(JSON.stringify(newParams));
					res.render('upload', { link: params.zip });
				});
			} else {
				res.render('uploadError');
			}

		});
	});

});

module.exports = router;